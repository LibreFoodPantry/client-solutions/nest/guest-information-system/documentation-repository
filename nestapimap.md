|**Guest Info System collections**|<p>Put </p><p>One</p>|Get One|Post All|Post One|Delete All|<p>Delete </p><p>One</p>|Put All|Put One|
| :- | :- | :- | :- | :- | :- | :- | :- | :- |
||||||||||
|**For the Guest Registration Entry collection**|||||||||
|Storing initial GRE data (**one**)||||Y|||||
|Storing initial GRE data (**all**)|||||||||
|Update **one** GRE entry  |Low|||||||Y|
|Retrieve **one** GRE entry||Y|||||||
|Retrieve **all** GRE entries|||||||||
|Delete **one** GRE entry||||||Y|||
|History for GRE|Low||||||||
||||||||||
|**For the QuestionaireSubmission collection**|||||||||
|Storing initial QS data (**one**)||||Y|||||
|Storing initial QS data (**all**)|||||||||
|Update **one** QS entry||||||||Y|
|Retrieve **one** QS entry||Y|||||||
|Retrieve **a range of** QS entries||Y|||||||
|Delete **one** QS entry||||||Y|||
||||||||||
| |||||||||
