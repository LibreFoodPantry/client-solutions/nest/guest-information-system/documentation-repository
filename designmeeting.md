** This doicuments an intial discussing with Dr. Burdge from Nassau Community College on 2/15/22 defining her requiremetns for our API **

Hello Dr. Burdge,

Thanks for meeting with me yesterday. This is a summation of what we discussed in the meeting.

1. There will be 2 mongoDB collections we will create modeling the 2 SQLite tables in the NEST android app with the same names, which are assigned to our "WSU Guest-Information Backend Team".

    GuestRegistrationEntry - This contains the basic demographic information for a guest, and will contain the elements listed in appendix A, using the contents of the barCode as an index.
    QuestionaireSubmissions - This contains data entered by the user filling out the questionnaire (containing the elements listed in appendix B.  Since the user is entering the same barcode they entered when they originally registered, this barcode value is to be used as a key to association the 2 collections, and will be passed into the PostQuestionaireSubmissions API call by NEST. as will the elements in appendix B.

2.  The attached word doc contains a table listing 12 use cases, which will be used initially to create our API.
3. The key we will be using is the contents of the user's barcode in String format. Both collections will include this value.
4. We should manufacture our own test data.
5. Security keys have not been determined yet, but we will coordinate with the IAM team to obtain this key.
6. Whether or not we will maintain a history of data for GRE and/or QS needs to be determined.
7. All data sent to us will be returned to NEST as it was sent. We will not be editing content in the back end. We will be returning HTTP system errors or schema validation errors when appropriate.

appendix A: (GRE)   - 28 strings

  public GuestRegistryEntry(
String name, String phone, String nccID, String date, String address, String city, String zipcode, String state, String affiliation, String age, String gender, String diet, String programs, String snap, String employment, String health, String housing, String income, String householdNum, String childcareStatus, String children1, String children5, String children12, String children18, String referralInfo, String additionalInfo, String nameOfVolunteer, String barcode) {

appendix B: (QS)  8 string ( including barcode ) and 1 long element
    public QuestionnaireSubmission(long rowID, String guestID, String adults, String seniors, String children, String firstVisit, String date, String visitCounter)
+ String barcode

ISSUES:

1. Since we will be using the barcode as a key for QS, and the data described in appendix B, is not including this,
this value should be passed to us also.
2.Security key to be determined.
3. Additional API changes will be needed if data history is needed.
4. Since a mongoDB collection is created if a JSON string is added to it the first time, using a PUSH call the first time should take care of any initialization. In a SQL world where Create, Open, Close, and Delete database calls are frequently used, I don't think we need this overhead but am not sure of this.

Please let me know if I am missing anything here, or if I got anything wrong.

Sincerely,
Joe Barry

CONTINUED: 2/19/22

Hello again,

 

I’ll try to outline the questions/feedback that I have. We can meet if you think it makes sense.

 

    The GRE also has a field called _ID which is an int.
    The QS field you’ve called rowID should also be an int.
    In QS there is a field called guestID (this is the barcode) – yes, I know, but two different teams worked on these, so naming conventions are not aligned; thus, the QS post does not need the bar code sent as well
    My questions about the table in the Word doc:
        I’m not sure what you mean by (one) vs (all) when posting guest registration data or questionnaire submissions data:

 

 

In both cases, we need to post one new record.

 

        I don’t recall the conversation, but after further review, we do not need “Update one QS entry”.
        I believe that we did talk about having the ability to retrieve QS data that fell within a data range, but I don’t see that here.
        Otherwise, this looks good!!
    Regarding 6. – the update for GRE is a low priority, maintaining a history of any changes to this data is an even lower priority; thus, the decision can be based on time constraints you are facing.
    Regarding ISSUES: 4. – I’m not sure that I understand what your concern is.

 

Hopefully this helps!!

 

Best,

Prof. Burdge



Thanks! A couple of answers/questions:

 

    The id/rowID fields are automatically added to each record that is written to the local SQLite database.
    Both tables will have a barcode and yes, we will use this field to associate the two tables. The id/rowID does not provide any association.
    For the following you say that you will “add this”, but I think you meant remove it?
        I don’t recall the conversation, but after further review, we do not need “Update one QS entry”.
        I will add this.

Otherwise, this looks good!

 

Best,

Prof. Burdge

NOTE: See nestapimap.docx for correct the actual api map

